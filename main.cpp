#include <iostream>
#include <memory>
#include <string>
#include <algorithm>
#include <iomanip>
#include "figures/figures.h"

int main() {
  std::string figure_type;
  std::cout << "Figure type (circle, triangle, rectangular): ";
  std::cin >> figure_type;

  std::array<std::string, 3> correct_types = {"circle", "triangle", "rectangular"};
  if (std::find(correct_types.begin(), correct_types.end(), figure_type) == correct_types.end()) {
    std::cout << "Incorrect figure type" << std::endl;
    return 1;
  }

  std::unique_ptr<figure::Figure> figure;

  auto read_value = [](const std::string& varname) {
    double x = 0;
    std::cout << varname + ": ";
    std::cin >> x;
    return x;
  };

  if (figure_type == "circle") {
    double x = read_value("x");
    double y = read_value("y");
    double radius = read_value("radius");

    if (radius < 0) {
      std::cout << "radius cannot be negative" << std::endl;
      return 1;
    }

    figure = std::make_unique<figure::Circle>(x, y, radius);
  } else if (figure_type == "triangle") {
    double x1 = read_value("x1");
    double y1 = read_value("y1");
    double x2 = read_value("x2");
    double y2 = read_value("y2");
    double x3 = read_value("x3");
    double y3 = read_value("y3");
    figure = std::make_unique<figure::Triangle>(x1, x2, x3, y1, y2, y3);
  } else /* if (figure_type == "rectangular") */ {
    double x_min = read_value("x min");
    double x_max = read_value("x max");
    double y_min = read_value("y min");
    double y_max = read_value("y max");

    if (x_min > x_max) {
      std::cout << "x min > x max is not valid" << std::endl;
      return 1;
    }

    if (y_min > y_max) {
      std::cout << "y min > y max is not valid" << std::endl;
      return 1;
    }

    figure = std::make_unique<figure::Rectangular>(x_min, x_max, y_min, y_max);
  }

  std::cout << "Area: " << std::setprecision(3) << figure->Area() << std::endl;
  return 0;
}
