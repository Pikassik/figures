#pragma once

namespace figure {

class Figure {
public:
  virtual double Area() = 0;
};

class Circle : public Figure {
public:
  Circle(double x_center, double y_center, double radius);

  double Area() override;

private:
  double x_center_;
  double y_center_;
  double radius_;
};

class Triangle : public Figure {
public:
  Triangle(double x1, double x2, double x3, double y1, double y2, double y3);

  double Area() override;

private:
  double x1_, x2_, x3_;
  double y1_, y2_, y3_;
};

class Rectangular : public Figure {
public:
  Rectangular(double x_min, double x_max, double y_min, double y_max);
  double Area() override;
private:
  double x_min_, x_max_;
  double y_min_, y_max_;
};

} // namespace figure