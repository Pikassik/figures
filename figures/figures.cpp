#include "figures.h"
#include <cmath>

namespace figure {

double CrossProduct(double x1, double x2, double y1, double y2) {
  return x1 * y2 - x2 * y1;
}

Circle::Circle(double x_center, double y_center, double radius)
        : x_center_(x_center), y_center_(y_center), radius_(radius) {}

double Circle::Area() {
  return M_PI * radius_ * radius_;
}

Triangle::Triangle(double x1, double x2, double x3, double y1, double y2, double y3)
        : x1_(x1), x2_(x2), x3_(x3), y1_(y1), y2_(y2), y3_(y3) {}

double Triangle::Area() {
  return std::abs(CrossProduct(x2_ - x1_, x3_ - x1_, y2_ - y1_, y3_ - y1_)) / 2;
}

Rectangular::Rectangular(double x_min, double x_max, double y_min, double y_max)
        : x_min_(x_min), x_max_(x_max), y_min_(y_min), y_max_(y_max) {}

double Rectangular::Area() {
  return (x_max_ - x_min_) * (y_max_ - y_min_);
}

} // namespace figure